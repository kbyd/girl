package com.cn.exception;

/**
 * Created by WUYN on 2017/11/3.
 */
public class GirlException extends RuntimeException {

    private String code;

    public GirlException(String code,String message) {
        super(message);
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
