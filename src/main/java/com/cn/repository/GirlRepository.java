package com.cn.repository;

import com.cn.domain.Girl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by WUYN on 2017/5/8.
 */
public interface GirlRepository extends JpaRepository<Girl,Integer> {

    //根据年龄查询
    public List<Girl> findByAge(Integer age);
}
