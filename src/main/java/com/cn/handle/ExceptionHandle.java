package com.cn.handle;

import com.cn.domain.Result;
import com.cn.exception.GirlException;
import com.cn.util.ResultUtil;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by WUYN on 2017/11/3.
 */
@ControllerAdvice
public class ExceptionHandle {

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result handle(Exception e){

        if(e instanceof GirlException){
            return ResultUtil.error(((GirlException) e).getCode(),e.getMessage());
        }else{
            return ResultUtil.error("02","未知错误");
        }

    }

}
