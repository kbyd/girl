package com.cn.controller;

import com.cn.property.GirlProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by WUYN on 2017/5/7.
 */

@Controller
@ResponseBody
public class HelloController {

    @Autowired
    private GirlProperties girlProperties;
    @Value("${content}")
    private String content;

    @RequestMapping(value = "/hello/{id}",method = RequestMethod.GET)
    public String say(@PathVariable("id") Integer id){
       // return girlProperties.getCupSize();
        return "id:"+id;
    }
}
