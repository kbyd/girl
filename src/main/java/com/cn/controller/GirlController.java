package com.cn.controller;

import com.cn.domain.Girl;
import com.cn.domain.Result;
import com.cn.repository.GirlRepository;
import com.cn.service.GirlService;
import com.cn.util.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by WUYN on 2017/5/8.
 */
@RestController
public class GirlController {

    @Autowired
    private GirlRepository girlRepository;

    @Autowired
    private GirlService girlService;

    //获取女生列表
    @GetMapping(value ="/girls")
    public List<Girl> girlList(){

        return girlRepository.findAll();
    }

    //添加一个女生
    @PostMapping(value = "/addGirl")
    public Result addGirl(@Validated Girl girl, BindingResult bindingResult){
        if(bindingResult.hasErrors()){

            System.out.println(bindingResult.getFieldError().getDefaultMessage());
            return ResultUtil.error("01",bindingResult.getFieldError().getDefaultMessage());
        }

        return ResultUtil.success(girlRepository.save(girl));

    }

    /**
     * 查询一个女生
     * @return
     */
    @GetMapping(value = "/girls/{id}")
    public Girl findGirl(@PathVariable("id") Integer id){

        return girlRepository.findOne(id);
    }

    //更新一个女生
    @PutMapping(value = "/girls/{id}")
    public Girl updateGirl(@PathVariable("id") Integer id,@RequestParam("cupSize") String cupSize,@RequestParam("age") Integer age){

        Girl girl = new Girl();
        girl.setId(id);
        girl.setCupSize(cupSize);
        girl.setAge(age);
        return girlRepository.save(girl);
    }

    //删除一个女生
    @DeleteMapping(value = "/girls/{id}")
    public void deleteGirl(@PathVariable("id") Integer id){

        girlRepository.delete(id);
    }

    //根据年龄查询
    @GetMapping(value = "/girls/age/{age}")
    public List<Girl> girlListByAge(@PathVariable("age") Integer age) throws Exception{

        //return girlRepository.findByAge(age);
        return girlService.getGirl(age);

    }

    //同时添加两个实体
    @PostMapping(value = "/girls/addTwo")
    public void addTwo(){
        girlService.insertTwo();
    }
}
