package com.cn.service;

import com.cn.domain.Girl;
import com.cn.exception.GirlException;
import com.cn.repository.GirlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by WUYN on 2017/10/24.
 */
@Service
public class GirlService {

    @Autowired
    private GirlRepository girlRepository;

    /**
     * @Description
     * @Author: WUYN
     * @Date: 2017/10/25 20:20
     * 
     */
    @Transactional
    public void insertTwo(){
        Girl girl = new Girl();
        girl.setAge(23);
        girl.setCupSize("E");
        girlRepository.save(girl);

        Girl girlRecord = new Girl();
        girlRecord.setAge(25);
        girlRecord.setCupSize("FFF");
        girlRepository.save(girlRecord);
    }

    public List<Girl> getGirl(Integer age) throws Exception{

        List<Girl> girls = girlRepository.findByAge(age);
        Girl girl = girls.get(0);
        if(girl.getAge()<10){
            throw new GirlException("01","可能在上小学");
        }
        return girls;
    }
}
