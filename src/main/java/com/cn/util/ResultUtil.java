package com.cn.util;

import com.cn.domain.Result;

/**
 * Created by WUYN on 2017/11/3.
 */
public class ResultUtil {

    public static Result success(Object object){
        Result result = new Result();
        result.setCode("00");
        result.setMsg("成功");
        result.setData(object);
        return result;

    }

    public static Result error(String code,String msg){
        Result result = new Result();
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
